from django.conf.urls import url, patterns, include
from eden.views import index

urlpatterns = [
    url(r'^index/$', index, name='index')
]