from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from eden.forms import DocumentForm
from eden.models import Document

import requests
import json

service_url = "http://64.62.141.221:8888/classify"

import eden_site.settings as config

def index(request):

    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(docfile = request.FILES['docfile'])
            newdoc.save()
            
            print("endereco do arquivo:  " + config.PROJECT_ROOT +  newdoc.docfile.url)
            try:
                with open( config.PROJECT_ROOT + newdoc.docfile.url, "rb") as imageFile:
                    response = requests.post(service_url, files = {"photo": imageFile})
                    print(response.text)
                
                    classificated_fruit = json.loads(response.text)

                    if (classificated_fruit['class'] == "healthy"):
                        newdoc.classification = "Alta Qualidade!"
                    else:
                        newdoc.classification = "Baixa Qualidade!"

                print(newdoc.classification)

            except Exception as e:
                newdoc.classification = "Não foi possível classificar."
                print(newdoc.classification)
                print(e)

            newdoc.save()
            return HttpResponseRedirect(reverse('index'))
    else:
        form = DocumentForm()

    documents = Document.objects.all()

    return render(
        request,
        'index.html',
        {'documents': documents, 'form': form}
    )